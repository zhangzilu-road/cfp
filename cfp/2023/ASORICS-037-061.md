---
layout: cfp
organizers:
    - name: 侯清源
      id: 2019XXXXXX037
    - name: 占明明
      id: 2019XXXXXX061
title: Asian Symposium on Research in Computer Security
shorttitle: ASORICS
accept-chinese-submission: true
only-for-homework: true
---

# Call for Paper

Computer security is concerned with the protection of information in environments where there is a possibility of intrusion or malicious action. The aim of ASORICS is to further the progress of research and development in computer security by establishing a Asian community for bringing together academia and industry in this area. Progressively organized in a series of Asian countries, the symposium is confirmed as one of the biggest Asian conferences in computer security. Nowadays, the symposium has also explored the R&D directions on AI, machine learning, privacy-enhancing technology, network security, software, and hardware security, block-chain, smart contract, and real-world applied cryptography. We are looking for papers with high-quality, original and unpublished research contributions. 

## Topics of interest

Submissions are solicited in all areas related to computer security, including but not limited to:

|                                                              |                                                              |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| Access Control                                               | Anonymity and Censorship Resistance                          |
| Applied Cryptography                                         | Artificial Intelligence for Security                         |
| Audit and Accountability                                     | Authentication and Biometrics                                |
| Data and Computation Integrity                               | Database Security                                            |
| Digital Content Protection                                   | Digital Forensics                                            |
| Formal Methods for Security and Privacy                      | Hardware Security                                            |
| Information Hiding                                           | Identity Management                                          |
| Information Flow Control                                     | Information Security Governance and Management               |
| Intrusion Detection                                          | Language-based Security                                      |
| Malware and Unwanted Software                                | Network Security                                             |
| Phishing and Spam Prevention                                 | Privacy Technologies and Mechanisms                          |
| Risk Analysis and Management                                 | Secure Electronic Voting                                     |
| Security Economics and Metrics                               | Security and Privacy of Systems based on Machine Learning and A.I. |
| Security and Privacy for Mobile / Smartphone Platforms       | Security for Emerging Networks (e.g., Home Networks, IoT, Body-Area Networks, VANETs) |
| Security, Privacy, and Resilience for Large-Scale, Critical Infrastructures (e.g., Smart Grid, AirPorts, Ports) | Security and Privacy in Social Networks                      |
| Security and Privacy in Wireless and Cellular Communications | Software Security                                            |
| Systems Security                                             | Trustworthy Computing to Secure Networks and Systems         |
| Usable Security and Privacy                                  | Web Security                                                 |


# Submission Instructions

Submitted papers must not substantially overlap with papers that have been published or that are simultaneously submitted to a journal or a conference / workshop with proceedings. Submitted papers must follow the LNCS template from the time they are submitted. Submitted papers should be at most 16 pages (using 10-point font), excluding the bibliography and well-marked appendices, and at most 20 pages total.

## Important Dates

|                                |      DATe       |
| :----------------------------: | :-------------: |
|   **Full paper submission:**   | 22 January 2023 |
| **Early reject notification:** |  26 March 2023  |
|  **Notification to authors:**  |  2 April 2023   |


# Organizers

- Workshop co-chair(s): Qingyuan Hou, Mingming Zhan

