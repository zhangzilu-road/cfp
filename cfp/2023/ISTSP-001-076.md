---
layout: cfp
organizers:
 
  -
    name: 陈雅宁
    id: 2022xxxxxx001
  -
    name: 王镨迎
    id: 2022xxxxxx076
title: Internet-of-Things Security Threats and Security Protection
shorttitle: ISTSP
only-for-homework: true
---

### Overview

The basic concept of the Internet-of-Things is perception, control, transmission and intelligence. Technical tools can be used to realize the cooperative relationship between things, people and things and people to form a larger complex network based on a network of sensors, the Internet and a mobile network. The exposure of resource-limited IoT devices to Internet threats opens the door to a number of potential IoT security and privacy risks, such as attacks on IoT systems and unauthorized access to end-users' personal information.

Security risks and threats continue to increase as the Internet of Things (IoT) will penetrate almost all areas of society, such as retail, transport, healthcare, energy supply and smart cities, and this security weakness can have catastrophic effects on real users and the physical world. Such risks may undermine public confidence in the deployment of internet technologies. It is therefore urgent to explore and introduce security mechanisms, models and schemes that are better aligned with the real needs of the Internet of Things, in order to further ensure its security and reliability.

This workshop aims to provide an excellent venue to showcase and discuss security threats in the Internet of Things, as well as the technical challenges and latest developments related to privacy protection technology for Internet of Things security. The topics include but are not limited to the following:


- Hardware security for Internet-of-Things
- Malware for Internet-of-Things
- Internet-of-Things vulnerabilities
- Intrusion detection and prevention for Internet-of-Things
- Security for future IoT architectures and designs.
- Secure wireless communication protocols in the Internet-of-Things.
- Security measurement and visualization techniques for Internet-of-Things
- Secure crowdsourcing in Internet-of-Things.
- Security and privacy of wireless IoT systems based on machine learning.
- Internet-of-Things authentication
- Privacy in Internet-of-Things-based services and applications.
- Privacy-preserving data aggregation and analysis in Internet-of-Things applications.
- Privacy in mobile and wireless communications for Internet-of-Things.
- Privacy-preserving authentication in Internet-of-Things.


### Important dates

- Submission deadline: June 5, 2023

- Initial notification: June 6, 2023

- Final acceptance/rejection notification: June 18 , 2023

- Publication: As per the policy of journal



### Submission Guidelines

- **Language:** The manuscript can only be written in English, usually no more than 10 pages.
- **Paper Originality:** Submissions must be original and should not have been published previously or be under consideration for publication while being evaluated for this conference. 
- **File Format:** Submitted report drafts should include no less than 4 pages, *excluding* references. There are no page limits on the references. Papers must be formatted according to the [IFW- requirements].Papers must be in PDF format.
- **Paper Anomynity:** All submitted materials will go through a double-blind review process, so the author's name or affiliation should not appear on the title page, and the document should avoid showing its identity in the text and document metadata. All submissions should be properly anonymized; papers not properly anonymized may be rejected without review.
- **Double-blind Review:** ISTSP2023 uses a double-blind review process. Each submission will be reviewed by at least three independent program committee members. And your papers will be judged on novelty, significance, clarity, practical relevance, and potential impact.
- **Valid Contact:** Contributors must provide a valid email address and mobile phone number.
- **Attendance Responsibilities:** For each paper accepted by the conference, the author should promise that at least one author will attend the conference for communication. Papers not attending the meeting will not be published.




### Organization

- Organizer: Internet-of-Things Security Threats and Security Protection (ISTSP)

- Hosts: Sichuan University, School of Cyber Science and Engineering

- General Chairs:
  - Yaning Chen, Sichuan University
  - Puying Wang, Sichuan University

### Contacts

- Conference Papers Chairs:

Yaning Chen, Sichuan University, China

Puying Wang, Sichuan University, China

**(Only applicable when `only-for-homework` is `false`)**

- Invited talk: Kai Gao
