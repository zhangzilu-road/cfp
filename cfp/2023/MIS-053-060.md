---
layout: cfp
organizers:
    - name: 包晟宏
      id: 2022xxxxxx053
    - name: 胡浩东
      id: 20222xxxxx060
title: Multi-media Information Security
shorttitle: MIS
only-for-homework: true
---

# Call for Paper

- We invite researchers, academicians, professionals, and practitioners to submit original research papers, case studies, and state-of-the-art reviews on multimedia security for publication in Multi-media Information Security journal.

  In today's world, multimedia security is becoming an increasingly important issue as we are constantly generating, transmitting, and storing multimedia data. As a result, research in this field is critical to ensure the protection of sensitive information and prevent unauthorized access, modification, or distribution of multimedia content. We are particularly interested in papers that explore the following topics:

  - Cryptography for multimedia security: Recent advancements in encryption algorithms and protocols that can be used to protect multimedia content.
  - Watermarking and fingerprinting for multimedia: Techniques for embedding and detecting watermarks or fingerprints in multimedia content to ensure data integrity.
  - Multimedia forensics and anti-forensics: Methods for identifying and analyzing digital evidence in multimedia data and techniques for circumventing or disguising such evidence.
  - Steganography and covert communication: Strategies for hiding messages within multimedia data without being detected.
  - Multimedia encryption and decryption: Techniques for securing multimedia content during transmission or storage.
  - Security of social multimedia: Privacy and security issues related to social media platforms that support multimedia sharing.

# Submission Instructions

Submissions should be original and unpublished papers, not exceeding 40000 words, in the format specified on the Multi-media Information Security journal website. Papers will be peer-reviewed by an international program committee, and accepted papers will be published in the journal.

For more information, please visit the Multi-media Information Security website or contact the editorial office at 1317752986@qq.com.

We look forward to receiving your contributions!

## Important Dates

- Submission Deadline: April 10,2023
- Notification of Acceptance: April 11,2023
- Camera-Ready Paper Deadline: May 10,2023
- Journal Publication: June 11,2023

# Organizers

- Workshop co-chair(s): Shenghong Bao, Haodong Hu

