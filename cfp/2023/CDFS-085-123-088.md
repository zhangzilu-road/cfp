---
layout: cfp
organizers:
    - name: 曾雨潼
      id: 2019XXXXXX085
    - name: 蒋书熠
      id: 2019XXXXXX123
    - name: 李智浩
      id: 2019XXXXXX088
title: Cyber-crime Defense and Forensics Symposium
shorttitle: CDFS
only-for-homework: true
---

# Call for Paper

_China Chengdu_
_The 1st Cyber-crime Defense and Forensics Symposium_
ShuangLiu, Chengdu, April 3

The 1st Cyber-crime Defense and Forensics Symposium (CDFS) provides a forum for researchers, practitioners, and law enforcement professionals to exchange knowledge and ideas on digital forensics, cybercrime investigation, and related topics. The target audience includes academics, researchers, practitioners, and law enforcement professionals interested in the practical aspects of cybercrime investigation and digital forensics.

## Topics of Interest

We invite submissions of technical papers, case studies, and panel proposals on all aspects of cybercrime investigation and digital forensics, including but not limited to:

- Digital forensic investigation techniques and tools
- Network and system forensics
- Mobile and cloud forensics
- Cybercrime investigation and incident response
- Active and passive cybercrime defense techniques, tools and analysis
- Legal and ethical issues in cybercrime investigation
- Digital evidence analysis and presentation
- Cybercrime and digital forensics education and training


All submissions will be reviewed by the Program Committee, and accepted papers will be published in the conference proceedings. Authors are encouraged to write the abstract and introduction of their paper in a way that is accessible and compelling to a general audience.

Authors are also free to post the camera-ready versions of their papers on their personal pages and within their institutional repositories. Reproduction for commercial purposes is strictly prohibited and requires prior consent.

Join us at CDFS 2023 to share your latest research and ideas with fellow experts in cybercrime investigation and digital forensics.

# Submission Instructions

The Submitted papers must be original work. A paper with substantial overlap with the submission must neither be already published, nor be currently under review for publication in any other venue. The review process will be double-blind. The authors must remove their names and affiliations in the submitted document.

The submitted paper must be the PDF format. Each paper is limited to 8 pages (or 10 pages with over length charge) but not less than 12 pages including figures and references. Authors can refer to the [IEEE Computer Society Proceedings Format](https://www.ieee.org/conferences/publishing/templates.html) to prepare their papers. 


## Important Dates

- Title and abstract Submission 25 April 2023
- Full Paper Submission deadline 25 May 2023
- Notification deadline 23 June 2023
- Start of Workshop 10 July 2023
- End of Workshop 23 July 2023

# Organizers

- Workshop co-chair(s): Yutong Zeng, Shuyi Jiang, Zhihao Li