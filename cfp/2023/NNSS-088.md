---
layout: cfp
organizers:
    - name: 朱文俊
      id: 2022xxxxxx088
title: Neural Network and Secuiry Symposium 
shorttitle: NNSS
only-for-homework: false
---

# Call for Paper

Neural networks are a powerful machine learning technique that can handle complex data and tasks, such as image recognition, natural language processing, anomaly detection, etc. Cybersecurity is a very important field that involves methods and tools to protect systems, networks and data from digital attacks. With the increase in the scale and complexity of the Internet and cyber attacks, developing new cybersecurity tools has become very important, especially for Internet of Things (IoT) networks. Neural networks provide new opportunities and challenges for cybersecurity, such as how to use neural networks to improve the efficiency, robustness and interpretability of cybersecurity, and how to prevent neural networks themselves from being attacked by adversarial attacks, tampering or stealing.

The NNSS conference aims to promote academic exchange and collaboration in the field of neural networks and cybersecurity, showcase the latest research results and application cases, and discuss future research directions and challenges. We invite SCU students to submit original, unpublished research papers covering but not limited to the following topics:

- Applications of neural networks in cybersecurity tasks such as intrusion detection, malware detection, anomaly detection, etc.
- Applications of neural networks in cybersecurity technologies such as encryption, privacy protection, blockchain, etc.
- Security issues and solutions of neural networks in emerging network architectures such as IoT, edge computing, cloud computing, etc.
- Theory and practice of neural networks in adversarial attacks, adversarial defense, adversarial learning, etc.
- Research on interpretability, trustworthiness, verifiability, etc. of neural networks in cybersecurity
- New models, algorithms, frameworks and tools of neural networks in cybersecurity

# Submission Instructions

The review process will be double-blind. The authors must remove their names and
affiliations in the submitted document.

The submitted paper must be the PDF format.

- Papers should be written in 简体中文 or English and follow the ACM conference format template. See https://www.overleaf.com/latex/templates/acm-conference-proceedings-primary-article-template/wbvnghjbzwpc for details.
- Papers should have more than 4000 words and not exceed 10 pages (including references), with up to 5 pages of appendices (not counted for review).
- Papers should be submitted through SCU mailbox zhuwenjun@stu.scu.edu.cn.
- Papers should be original, unpublished or under submission to other conferences or journals.
- Papers will be reviewed through a peer review process based on quality, originality, relevance and readability.

## Important Dates

- Title and abstract Submission 30 April 2023
- Full Paper Submission deadline 30 May 2023
- Notification deadline 28 June 2023
- Start of Workshop 15 July 2023
- End of Workshop 30 July 2023

# Organizers

- Workshop co-chair(s): Wenjun Zhu，Sichuan University

**(Only applicable when `only-for-homework` is `false`)**

