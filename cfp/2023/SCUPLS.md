---
layout: cfp
organizers:
- name: 李俊杰
  id: 2022xxxxxx061
- name: 杨家豪
  id: 2022xxxxxx081
title: "The 1st SCU Physical Layer Security Conference"
shorttitile: SCUPLA 2023 
only-for-homework: true
accept-chinese-submission: true
---

# Call for Papers
## SCUPLA 2023

Physical layer security is becoming an increasingly important topic in the field of information security. It refers to the use of physical properties of the communication channel to achieve secure and reliable communication between two parties. This technique is based on the principle that every communication channel has inherent noise that can be exploited to generate secret keys, verify the identity of the communicating parties, and ensure the confidentiality, integrity, and availability of the transmitted data.Recent advances in wireless and mobile communications, Internet of Things (IoT), and 5G networks have led to significant challenges in securing the physical layer. Attacker can launch various types of attacks by exploiting the wireless channel characteristics, such as eavesdropping, jamming, spoofing, and denial-of-service (DoS) attacks. To face these challenges, physical layer security is attracting more and more attention from researchers in academia and industry. 

## IMPORTANT DATES

Paper submission deadline: June 1, 2023
Author notification: August 1, 2023
Camera-ready submission: September 1, 2023
Conference date: October 1-3, 2023


## Symposium Topics

In this call for papers, Physical Layer Security Organizing Committee invite submissions from researchers, practitioners, and students around the world who are interested in physical layer security. We solicit original contributions, both theoretical and practical, that address the following topics:
1. Physical Layer Security Models: Development of mathematical models that assess the security of wireless and wireline networks at the physical layer.
2. Physical Layer Cryptography: Study, design, and implementation of cryptographic primitives based on the physical properties of the communication channel, such as channel reciprocity, randomness, and fading effects.
3. Secret Key Generation: Techniques and algorithms for key generation, agreement, and distribution based on the physical layer characteristics, such as channel measurements and channel state information (CSI).
4. Authentication and Verification: Methods and approaches for identity verification and authentication of the communicating parties at the physical layer, using channel parameters, signal strength, or time-of-arrival information.
5. Jamming and Spoofing Mitigation: Countermeasures and defense mechanisms against jamming and spoofing attacks, such as adaptive modulation and coding, frequency hopping, beamforming, and multiple antenna techniques.
6. Experimental Evaluations and Testbeds: Experimental studies and validations of physical layer security techniques and protocols, using real-world or simulated testbeds.
We welcome papers that present novel and practical solutions, as well as theoretical papers that advance the state-of-the-art in physical layer security. All papers will undergo a rigorous review process, and accepted papers will be published in the conference proceedings.

## Submission Guidelines

Submissions must be original and should not have been published previously or currently submitted for publication elsewhere.
Papers should be limited to 6 pages, including references and appendices. 
Submissions should conform to the IEEE Conference Proceedings format, with 10-point font size, and papers should be submitted in PDF format.

## Contact Information

If you have any questions or need more information, please contact us at:2504295084@qq.com