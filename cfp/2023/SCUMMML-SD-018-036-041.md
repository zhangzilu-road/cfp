---
layout: cfp
organizers:
- name: 丁一凡
  id: 2022xxxxxx018
- name: 雷颖
  id: 2022xxxxxx041
- name: 曹蓓
  id: 2022xxxxxx036
title: "The 1st SCU Symposium on Multimodal Machine Learning for Sentiment Detection"
shorttitile: SCUMMML-SD 2023
only-for-homework: true
accept-chinese-submission: true
---

# Annoucement and Call for Papers

**The 1st SCU Symposium on Multimodal Machine Learning for Sentiment Detection**

**June 7, 2023, Chengdu, China**

*Sponsored by unnamed scholars*

## Symposium Overview

The 1st SCU Symposium on Multimodal Machine Learning for Sentiment Detection We are delighted to announce The 1st SCU Symposium on Multimodal Machine Learning for Sentiment Detection, which will be held virtually on July 20-22, 2023. The symposium aims to provide a forum for researchers, practitioners, and industry professionals to share their latest research findings and ideas on the application of multimodal machine learning techniques in sentiment detection.

Please note that the deadline for paper submission is April 30, 2023. We highly recommend that you submit your paper at your earliest convenience, to ensure ample time for us to arrange the review process and prepare the proceedings.

## Important Dates

• Paper submission deadline: April 30, 2023

• Notification of acceptance: June 1, 2023

• Camera-ready paper deadline: June 30, 2023

• Symposium dates: July 20-22, 2023
## Symposium Organizers 
The symposium is organized by the Academy of Cyberspace Security, Sichuan University, China. The organizing committee consists of the following members:

Ding Yifan, Sichuan University

Lei Ying, Sichuan University

Cao Bei, Sichuan University

## Symposium Topics

Multimodal machine learning is a technique that involves combining information from multiple modalities, such as text, image, audio, and video, to improve the accuracy and robustness of machine learning models. In natural language processing and sentiment analysis, incorporating multimodal features can provide a more comprehensive understanding of the sentiment expressed in a given text, as well as provide insights into the context and underlying emotions. Multimodal machine learning has become an increasingly important area of research, as it has the potential to enhance the performance of various applications, such as customer feedback analysis, social media sentiment analysis, and news and review sentiment detection.

The symposium welcomes submissions on a wide range of topics related to multimodal machine learning for sentiment detection, including but not limited to:

Multimodal sentiment analysis
Multimodal feature fusion for sentiment detection
Multimodal deep learning for sentiment detection
Multimodal sentiment classification in social media
Multimodal sentiment detection in customer feedback analysis
Multimodal sentiment detection in news and reviews

## Submission Policies

The symposium invites submissions of original research papers and posters. All submissions will be subject to a rigorous peer-review process. Papers should be formatted according to the ACM Conference Proceedings format and should not exceed 8 pages, including references. Posters should be no larger than 36 inches wide and 48 inches tall. All accepted papers and posters will be published in the symposium proceedings, which will be included in the ACM Digital Library.

We look forward to your participation in The 1st SCU Symposium on Multimodal Machine Learning for Sentiment Detection. Please feel free to contact us if you have any questions or concerns.

We look forward to your participation and contribution, and to exploring the latest research progress in multimodal machine learning and sentiment detection with you. If you have any questions or need further information about the symposium, please feel free to contact us at xxxx@scu.edu.cn.

