---
layout: cfp
organizers:
    - name: 杨榉栋
      id: 2022XXXXXX024
    - name: 袁祖超
      id: 2022XXXXXX083
title: International Conference on Artificial Intelligence 2023 
shorttitle: AI2023
accept-chinese-submission: true
only-for-homework: true
---

# Call for Paper

The International Conference on Artificial Intelligence 2023 (AI2023) aims to provide an international platform for researchers, scholars and engineers to exchange the latest research results and discuss future directions. The theme of the conference is "Artificial Intelligence and the Future Smart Society", covering various fields of AI and its applications in the construction of a smart society. The conference will invite famous scholars and enterprise representatives from home and abroad to give keynote speeches, and set up several sub-forums, welcome scholars and engineers to contribute and attend the conference.

## Topics of interest

Submissions are solicited in all areas related to artificial intelligence, including but not limited to:

* Machine Learning and Deep Learning
* Natural Language Processing
* Artificial Intelligence Algorithms and Models
* Machine Vision
* Data Mining and Knowledge Discovery
* Intelligent Internet of Things and Smart Manufacturing
* Human-Computer Interaction and Intelligent Transportation
* Artificial Intelligence and Health Care


# Submission Instructions

1. The conference accepts submissions of papers and technical reports in Chinese or English, with a maximum length of 10 pages (including references). 
2. All papers are subject to strict peer review, and accepted papers will be included in the conference proceedings and published by the publisher. Some high level papers will be recommended for publication in reputable journals.
3. The deadline for submission of papers is May 1, 2023.
4. All submitted articles should be original works, no plagiarism or plagiarism.

## Important Dates

| IMPORTANT DATE | DATE |
| :--: | :--: |
| Deadline | May 1, 2023 |
| Notification of acceptance | May 25, 2023 |
| Deadline for final version | June 10, 2023  |
| Conference Dates | June 20, 2023 - June 25, 2023 |


# Organizers

- Workshop co-chair(s): Judong Yang, ZhuChao Yuan