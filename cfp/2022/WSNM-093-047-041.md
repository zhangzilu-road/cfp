---
organizers:
  -
    name: 魏川程
    id: 2021XXXXXX093
  -
    name: 姜鑫涢
    id: 2021XXXXXX047
  -
    name: 刘丰源
    id: 2021XXXXXX041

title: Workshop on Social Networks Mining
shorttitle: WSNM
only-for-homework: false
accept-chinese-submission: true
final: true
---

## Call For Papers

When the first Social Networks were introduced in the beginning of this century,
they were able to gain the attention of the whole world, and since then, more
and more social networks have risen and have become increasingly popular and
significantly effective. They have been able to change the way we live forever
and how we interact with each other and with the environment. The world has
become more and more connected. Nowadays, social networks are everywhere, and
they play an important role in various aspects of our lives. With the rise of
the era of social networks, many opportunities and challenges have appeared on
the scene including but not limited to big data handling, security and privacy
issues in social networks, the use of sentiment analysis and opinion mining in
these rich environments, data analytics, recommender systems, and legal and
ethical issues related to the offensive use of these environments.

The objective of this workshop is to shed light upon all the issues,
opportunities, challenges, and future trends that are related to online social
networks. Authors are invited to submit their original work, which is not
submitted elsewhere, to this workshop.


## Topics of interest

Papers related to the conference theme will receive priority consideration,
including theories, methodologies, and emerging applications. We invite
contributions on theory and practice, including but not limited to the following
technical areas.

- Social Networks Mining and Analysis
- Social Networks Architecture
- Social Networks Data Representation and Visualization
- Decentralized Social Networking applications: P2P, Cloud, and Mobile
- IoT and Social Networks
- Cloud Mining tools and Services for Social Networks Data
- Geolocation aspects of Social networks
- Impact of Social Networks on Society
- Social Networks Sentiment Analysis and Opinion Minings
- Information Retrieval for Social Networks
- Multilingual Natural Language Processing Tools for Social Networks
- Recommender systems Applications in Social Networks


## Submission Guidelines
Authors are invited to submit original previously unpublished research papers written in English or Chinese, of up to 6 pages. Submissions not following the format guidelines will be rejected without review. To ensure high quality, all papers will be thoroughly reviewed by the WSNM 2022 Program Committee. All accepted papers must be presented by one of the authors who must register for the conference and pay the fee. 

Authors can download the Latex (recommended) or Word templates available below:

- English: [Templates for Conference Papers - USENIX](https://www.usenix.org/conferences/author-resources/paper-templates)
- Chinese: [四川大学学报 自然科学版](http://jsuese.ijournals.cn/uploadfile/news_images/jsuese_cn/2021-03-04/%E3%80%8A%E5%B7%A5%E7%A8%8B%E7%A7%91%E5%AD%A6%E4%B8%8E%E6%8A%80%E6%9C%AF%E3%80%8B%E8%AE%BA%E6%96%87%E6%8A%95%E7%A8%BF%E6%A8%A1%E6%9D%BF.doc)


## Submission Site

Please submit your paper to email on time **vchopin@qq.com**.

**Welcome to contribute!**

## Important Dates
- Paper Submission: May 24, 2022
- Acceptance Notification: June 1, 2022

## Organizer
Sichuan University，School of Cyber Science and Engineering

## Program Chairperson/Seminar Chairperson
Haizhou Wang, Sichuan University, China

Gang Liang, Sichuan University, China

Yizhou Li, Sichuan University, China

## Program Committee
Chuancheng Wei, Sichuan University, China

Xinyun Jiang, Sichuan University, China

Fengyuan Liu, Sichuan University, China
