---
layout: cfp
organizers:
  - name: 何宇浩
    id: 2021XXXXXX061
  - name: 杨鹏
    id: 2021XXXXXX063
  - name: 牟雨萌
    id: 2021XXXXXX042
title: Machine Learning in Cyberspace security
shorttitle: MLCS
only-for-homework: true
---

# Call For Papers
## __| MLCS 2022 |__
  
_The 1st Meeting of Machine Learing in Cyberspace Security  
May 25-27, 2022_  
___ShuangLiu, ChengDu, China___

---

In today's increasingly interdependent global world, Cyberspace Security has gradually become the focus of public attention. With the expansion of network scale, due to the lack of consideration of security and reliability at the beginning of network structure and protocol design, information transmission and data protection have been greatly threatened. Cyberspace Security mainly includes three levels: system security, network security and application security. The traditional security solutions become inefficient in the face of massive data. With the birth of emerging technologies, more technical means need to be introduced. Among them, machine learning is a powerful and effective method. With its strong adaptability and self-learning ability, it provides a series of effective analysis and decision-making tools for the security field, which has attracted extensive attention and in-depth research in academia and industry in recent years.  

The objective of MLCs 2022 workshop is to bring researchers and practitioners together to share how they can apply machine learning based defense methods to the field of Cyberspace Security.Contributions on machine learning, deep learning, and data-driven approaches in general to the problems in cyber threat detection, prediction and defense issues are particularly welcome.  

Although not limited, papers are solicited for the following main topics:

+ __System hardware and physical environment security__  
Hardware equipment identity authentication is a common security problem of system hardware. The application of machine learning in equipment identity authentication technology mainly includes three kinds of fingerprint features: transient signal, modulation signal and spectrum response. In the physical environment, machine learning mainly studies channel attack and pseudo base station detection.
+ __system software security__  
The research of machine learning in system software security includes vulnerability analysis and mining, malicious code analysis, user identity authentication and virtualization security.
+ __network infrastructure security__  
Routing system and domain name system are important network infrastructure in cyberspace.The research of machine learning in network infrastructure security includes BGP anomaly detection and DNS malicious attack detection.
+ __network security detection__  
Application of machine learning technology and network security detection include botnet detection, network intrusion technology and identification of malicious encrypted traffic.
+ __application software security__  
E-mail, PDF and web pages are the most common software applications. The research of machine learning technology in application software security includes: spam detection, malicious web page identification and malicious PDF document detection.
+ __social network security__  
The research of machine learning technology in social network security includes: social network abnormal account detection, credit card fraud detection, forensics analysis and network public opinion analysis.

## __Submission Guidelines__

MLCS 2022 welcomes original, unpublished papers. All submissions must be written in English.  

Papers submitted for review must clearly state: 

+ The purpose of the work
+ How and to what extent it advances the state-of-the art 
+ Specific results and their impact
+ Papers must not exceed four A4 pages with all illustrations and references included.

## __Important Dates__

Papers Submission Deadline: April 15, 2022  
Decision Notification: April 22, 2022

## __VENUE AND CONTACT DETAILS__
 
MLCS 2022 workshop will be held on May 25-27, 2022. We will let you know when the exact location info becomes available.

### __ORGANIZING COMMITTEE__

He Yuhao, Sichuan University  
Yang Peng, Sichuan University  
Mou Yumeng, Sichuan University
