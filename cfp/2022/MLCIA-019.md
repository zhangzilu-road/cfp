---
organizers:
  -
    name: 陈洪森
    id: 2021XXXXXXX019

title: International Conference on Machine Learning Confidentiality, Integrity and Availability
shorttitle: MLCIA
only-for-homework: true
accept-chinese-submission: true
score1:
score2:
comments: ""
---

## The 1th International Conference on Machine Learning Confidentiality, Integrity and Availability

---
Although machine learning model has excellent performance in practical application, it still faces many security threats. The confidentiality, integrity, and availability of machine models are threatened to varying degrees. The security and privacy threats faced by machine learning at data layer, model layer and application layer are characterized by diversity, concealness and dynamic evolution.

The purposes of this special issue are  :(1)to present research on the confidentiality, integrity, and usability of machine learning; (2) to provide a platform for researchers and practitioners to promote more and more people to pay attention to relevant research.

Topics of interest include, but are not limited to:
- Model Poisoning attack and defense
- Model Evasion attacks and defense
- Model inversion
- AI backdoors
- Membership inference attacks
- Federated learning
- Digital watermarking for AI models
- Privacy-preserving machine learning
- Break Model availability
- Interpretability of machine learning models
- Secure model processing platforms
- Privacy-preserving machine learning techniques

Submitted articles must not have been previously published or currently submitted for conference/journal publication elsewhere.

### Paper Categories
Full papers, presenting novel and mature research results, are limited to 20 pages in double-column [ACM CCS format](https://www.overleaf.com/latex/templates/sample-acm-ccs/hqrzvbjgvfvz), including bibliography and appendices.

Papers that do not follow the above formatting guidelines may be rejected without review.

### Important Dates
- Submission Deadline: 15 July  2022
- Acceptance Notification: 30 June 2022
- Presentation at Conference: 15 August 2022
