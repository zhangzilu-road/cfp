--- 
organizers:
  -
    name: 问欣
    id: 2021XXXXXXX067
title: Advance of Fuzzing of SiChuan University
shorttitle: AFSCU
only-for-homework: true
accept-chinese-submission: true

---


## Call For Papers
At present, Internet technology has occupied a vital position in political, economic, cultural, military and other fields. However, with the expansion of the application scope of Internet, the problem of network security has become increasingly prominent. Hackers use the vulnerabilities in the program to invade the network, destroy the system and steal information, which seriously endangers network security. Fuzz testing is one of the most effective vulnerability detection technologies at present. In the past 10 years, researchers have used this technology to find thousands of vulnerabilities in applications, OS kernel, protocols, smart contracts, Internet of things systems, databases, compilers and others. Fuzz testing shows great development potential. This special issue focuses on the research and development related to fuzz testing, and aims to solicit original research papers which discuss the latest research results, analysis methods, technologies, and applications in the field of fuzz testing. We welcome all researchers, scientists, engineers and practitioners who study fuzz testing to contribute. 
## Topics of interest
- Seed set selection
- Seed Schedule
- Byte Schedule
- Mutation Operator Schedule
- Diverse Information For Fitness
- Evaluation Theory
- Theory of searching inputs space
- Automatic Execution of PUTs
- Automatic Detection of Bugs
- Improvement of Execution Speed
## Submission Guidelines
This special issue accepts two ways of submission. You can choose to submit drafts or full papers. A draft is a full paper sans the evaluation or experiments. If the draft is accepted, you need to submit the full paper before the final submission deadline. Each submission will be reviewed by at least three committee members with the key objective of providing constructive feedback. Submitted drafts should include no more than 8 pages, excluding references. There are no page limits on the references. Papers must be formatted according to the AFSCU requirements. Templates are available in AFSCU official website.
## Important Dates
- Drafts/Full Paper Submission Deadline: March 31, 2022
- Notification of Acceptance/Rejection: April 14, 2022
- Final Paper Submission Deadline: August 26, 2022
- Publication: September 27, 2022